﻿using TcmbCurrenciesProject.Data;
using TcmbCurrenciesProject.Entity;
using System;
using System.Collections.Generic;
using System.Linq;

namespace TcmbCurrenciesProject.Business
{
    public class CurrencyService : ICurrencyService
    {
        private readonly ICurrencyRepo _repo;

        public CurrencyService(ICurrencyRepo repo)
        {
            _repo = repo;
        }

        public List<CurrencyRate> GetDateCurrencies(DateTime date)
        {
            CheckDate(date);
            List<CurrencyRate> rates = _repo.GetDateCurrencyRates(date);
            if (rates == null || rates.Count == 0)
            {
                rates = TcmbHelper.GetCurrencyRatesByTcmb(date)
                    .Select(x => new CurrencyRate()
                    {
                        Currency = x.Name,
                        Rate = x.Rate,
                        Date = date
                    })
                    .ToList();
                _repo.SaveCurrencyRates(rates);
            }
            return rates;
        }

        private void CheckDate(DateTime date) 
        {
            if (date > DateTime.Today)
            {
                throw new Exception("Aranan kur bugünden daha ileriki bir tarihe ait olamaz.");
            }

            if (HolidayHelper.IsHoliday(date))
            {
                throw new Exception("Aranılan gün tatil günü.");
            }
        }
    }
}
