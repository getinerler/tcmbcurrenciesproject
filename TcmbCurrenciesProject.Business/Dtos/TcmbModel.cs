﻿namespace TcmbCurrenciesProject.Business.Dtos
{
    public class TcmbModel
    {
        public string Name { get; set; }
        public decimal Rate { get; set; }
    }
}
