﻿using System;

namespace TcmbCurrenciesProject.Business
{
    public static class HolidayHelper
    {
        public static bool IsHoliday(DateTime date)
        {
            if (IsWeekend(date) || IsPrivateDay(date))
            {
                return true;
            }
            return false;
        }

        //Gerçek bir projede metot yıldan yıla değişen kurban bayramı gibi tatiller için genişletilebilir.
        //Veritabanına elle değişen tatiller her yıl girilir, burada kontrol yapılır, örneğini gördüm.
        private static bool IsPrivateDay(DateTime date)
        {
            //29 Ekim
            if (date.Day == 29 && date.Month == 10)
            {
                return true;
            }
            //19 Mayıs
            if (date.Day == 19 && date.Month == 5)
            {
                return true;
            }
            //23 Nisan
            if (date.Day == 23 && date.Month == 4)
            {
                return true;
            }
            //30 Ağustos
            if (date.Day == 30 && date.Month == 8)
            {
                return true;
            }
            return false;
        }

        private static bool IsWeekend(DateTime date)
        {
            return date.DayOfWeek == DayOfWeek.Saturday || date.DayOfWeek == DayOfWeek.Sunday;
        }
    }
}
