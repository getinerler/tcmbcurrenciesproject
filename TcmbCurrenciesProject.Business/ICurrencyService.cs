﻿using TcmbCurrenciesProject.Entity;
using System;
using System.Collections.Generic;

namespace TcmbCurrenciesProject.Business
{
    public interface ICurrencyService
    {
        public List<CurrencyRate> GetDateCurrencies(DateTime date);       
    }
}
