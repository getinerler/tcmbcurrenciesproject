﻿using TcmbCurrenciesProject.Entity;
using System;
using System.Linq;
using System.Collections.Generic;

namespace TcmbCurrenciesProject.Data
{
    public class CurrencyRepo : ICurrencyRepo
    {
        private readonly DatabaseContext _ctx;

        public CurrencyRepo(DatabaseContext ctx)
        {
            this._ctx = ctx;
        }

        public List<CurrencyRate> GetDateCurrencyRates(DateTime date)
        {
            List<CurrencyRate> rates = _ctx.CurrencyRates
                .Where(x => x.Date == date)
                .ToList();
            return rates;
        }

        public void SaveCurrencyRates(List<CurrencyRate> rates)
        {
            _ctx.CurrencyRates.AddRange(rates);
            _ctx.SaveChanges();
        }
    }
}
