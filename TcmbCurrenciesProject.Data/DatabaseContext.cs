﻿using TcmbCurrenciesProject.Entity;
using Microsoft.EntityFrameworkCore;

namespace TcmbCurrenciesProject.Data
{
    public class DatabaseContext : DbContext
    {
        public DatabaseContext(DbContextOptions ops) : base(ops) 
        {

        }

        public DbSet<CurrencyRate> CurrencyRates { get; set; }
    }
}
