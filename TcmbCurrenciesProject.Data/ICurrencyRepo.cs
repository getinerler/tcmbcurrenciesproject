﻿using TcmbCurrenciesProject.Entity;
using System;
using System.Collections.Generic;

namespace TcmbCurrenciesProject.Data
{
    public interface ICurrencyRepo
    {
        public List<CurrencyRate> GetDateCurrencyRates(DateTime date);
        public void SaveCurrencyRates(List<CurrencyRate> rate);
    }
}
