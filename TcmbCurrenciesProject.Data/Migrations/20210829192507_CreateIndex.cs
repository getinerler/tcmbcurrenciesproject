﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace TcmbCurrenciesProject.Data.Migrations
{
    public partial class CreateIndex : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateIndex(
                name: "IX_CurrencyRates_Date",
                table: "CurrencyRates",
                column: "Date");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "IX_CurrencyRates_Date",
                table: "CurrencyRates");
        }
    }
}
