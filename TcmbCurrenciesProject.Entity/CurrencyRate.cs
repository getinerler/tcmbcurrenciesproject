﻿using Microsoft.EntityFrameworkCore;
using System;

namespace TcmbCurrenciesProject.Entity
{
    [Index(nameof(Date), IsUnique = false)]
    public class CurrencyRate
    {
        public int CurrencyRateId { get; set; }
        public DateTime Date { get; set; }
        public string Currency { get; set; }
        public decimal Rate { get; set; }
    }
}
