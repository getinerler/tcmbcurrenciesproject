﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using TcmbCurrenciesProject.Business;
using TcmbCurrenciesProject.Dtos;
using TcmbCurrenciesProject.Entity;

namespace TcmbCurrenciesProject.Controllers
{
    public class HomeController : Controller
    {
        private readonly ICurrencyService _service;

        public HomeController(ICurrencyService service)
        {
            _service = service;
        }

        public IActionResult Index()
        {
            return View();
        }

        public AjaxResponse<List<CurrencyRate>> ReadCurrencies(string date)
        {
            try
            {
                List<CurrencyRate> list = _service.GetDateCurrencies(date.ParseDate());
                return new AjaxResponse<List<CurrencyRate>>()
                {
                    IsSuccessful = true,
                    Data = list
                };
            }
            catch (Exception ex)
            {
                return new AjaxResponse<List<CurrencyRate>>()
                {
                    IsSuccessful = false,
                    Message = ex.Message
                };
            }
        }
    }
}
