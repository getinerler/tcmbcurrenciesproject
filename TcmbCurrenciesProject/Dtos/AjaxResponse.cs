﻿namespace TcmbCurrenciesProject.Dtos
{
    public class AjaxResponse<T>
    {
        public bool IsSuccessful { get; set; }
        public string Message { get; set; }
        public T Data { get; set; }
    }
}
