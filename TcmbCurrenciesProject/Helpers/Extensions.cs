﻿using System;

namespace TcmbCurrenciesProject
{
    public static class Extensions
    {
        public static DateTime ParseDate(this string date)
        {
            if (string.IsNullOrEmpty(date))
            {
                throw new Exception("Tarih metni yok.");
            }

            string[] list = date.Split('.');

            if (list.Length < 3)
            {
                throw new Exception("Tarih metni hatalı.");
            }

            if (!int.TryParse(list[0], out int day))
            {
                throw new Exception("Gün metni hatalı.");
            };

            if (!int.TryParse(list[1], out int month))
            {
                throw new Exception("Ay metni hatalı.");
            }

            if (!int.TryParse(list[2], out int year))
            {
                throw new Exception("Yıl metni hatalı.");
            }

            return new DateTime(year, month, day);
        }
    }
}
